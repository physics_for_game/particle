﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodFingerManager : MonoBehaviour {
    [SerializeField] float _pokeImpulseForceMagnitude = 10;
    [SerializeField] float _pokeNormalForceMagnitude = 5;
    [SerializeField] float _ballImpulseForce = 5;

    private bool _bLeftAlt, _bLeftCtrl;

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        _bLeftAlt = Input.GetKey (KeyCode.LeftAlt);
        _bLeftCtrl = Input.GetKey (KeyCode.LeftControl);

        if (Input.GetMouseButtonDown (0)) {
            ProcessLeftMouseClick ();
        }
    }

    private void ProcessLeftMouseClick () {
        //A RaycastHit information object
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
        if (Physics.Raycast (ray, out hit)) {
            if (hit.rigidbody != null) {
                Vector3 rayDir = ray.direction;
                rayDir = rayDir.normalized;

                if (!_bLeftCtrl) {
                    hit.rigidbody.AddForceAtPosition (rayDir *
                        _pokeImpulseForceMagnitude, hit.point, ForceMode.Impulse);
                } else {
                    hit.rigidbody.AddForceAtPosition (-rayDir *
                        _pokeImpulseForceMagnitude, hit.point, ForceMode.Impulse);
                }
            }
        }
    }
}